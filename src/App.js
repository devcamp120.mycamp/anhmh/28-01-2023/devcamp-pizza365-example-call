import 'bootstrap/dist/css/bootstrap.min.css'
import FetchAPI from './components/FetchAPI';

function App() {
  return (
    <div> 
      <FetchAPI/>
    </div>
  );
}

export default App;

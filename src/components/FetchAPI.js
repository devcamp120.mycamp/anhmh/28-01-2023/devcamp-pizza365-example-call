import { Component } from "react";

class FetchAPI extends Component {
    fetchAPI = async () => {
        let response = await fetch(url, requestOptions);
        let data = await response.json();

        return data
    }
    getAllOrdersHandler = () => {
        var requestOptions = {
            method: 'GET',
            redirect: 'follow'
        };

        var url = "http://42.115.221.44:8080/devcamp-pizza365/orders";

        this.fetchAPI(url, requestOptions)
            .then((response) => {
                console.log(response)

            })
            .catch((error) => {
                console.log(error)
            })
    }

    createOrderHandler = () => {
        var myHeaders = new Headers();
        myHeaders.append("Content-Type", "application/json");

        var raw = JSON.stringify({
            "kichCo": "M",
            "duongKinh": 25,
            "suon": 4,
            "salad": 300,
            "loaiPizza": "HAWAII",
            "idVourcher": "16512",
            "idLoaiNuocUong": "PEPSI",
            "soLuongNuoc": 3,
            "hoTen": "Phạm Thanh Bình",
            "thanhTien": 200000,
            "email": "binhpt001@devcamp.edu.vn",
            "soDienThoai": "0865241654",
            "diaChi": "Hà Nội",
            "loiNhan": "Pizza đế dày"
        });

        var requestOptions = {
            method: 'POST',
            headers: myHeaders,
            body: raw,
            redirect: 'follow'
        };

        var url = "http://42.115.221.44:8080/devcamp-pizza365/orders";

        this.fetchAPI(url, requestOptions)
            .then((response) => {
                console.log(response)

            })
            .catch((error) => {
                console.log(error)
            })

    }

    getOrderByIdHandler = () => {
        var requestOptions = {
            method: 'GET',
            redirect: 'follow'
        };

        var url = "http://42.115.221.44:8080/devcamp-pizza365/orders/1";

        this.fetchAPI(url, requestOptions)
            .then((response) => {
                console.log(response)

            })
            .catch((error) => {
                console.log(error)
            })
    }

    updateOrderHandler = () => {
        var myHeaders = new Headers();
        myHeaders.append("Content-Type", "application/json");

        var raw = JSON.stringify({
            "kichCo": "L",
            "duongKinh": 25,
            "suon": 4,
            "salad": 300,
            "loaiPizza": "SEAFOOD",
            "idVourcher": "16512",
            "idLoaiNuocUong": "PEPSI",
            "soLuongNuoc": 3,
            "hoTen": "Phạm Thanh Bình",
            "thanhTien": 200000,
            "email": "binhpt001@devcamp.edu.vn",
            "soDienThoai": "0865241654",
            "diaChi": "Hà Nội",
            "loiNhan": "Pizza đế dày"
        });

        var requestOptions = {
            method: 'PUT',
            headers: myHeaders,
            body: raw,
            redirect: 'follow'
        };

        var url = "http://42.115.221.44:8080/devcamp-pizza365/orders/1";

        this.fetchAPI(url, requestOptions)
            .then((response) => {
                console.log(response)

            })
            .catch((error) => {
                console.log(error)
            })

    }

    checkVoucherByIdHandler = () => {

    }

    getDrinkListHandler = () => {
        var requestOptions = {
            method: 'GET',
            redirect: 'follow'
        };

        var url = "http://42.115.221.44:8080/devcamp-pizza365/drinks";

        this.fetchAPI(url, requestOptions)
            .then((response) => {
                console.log(response)

            })
            .catch((error) => {
                console.log(error)
            })
    }

    render() {
        return (
            <div>
                <div className="container">
                    <div className="form-group">
                        <p id="cmt2dev" style={{ marginLeft: "-95px" }}> Test Page for Javascript Tasks. F5 to run code</p>
                    </div>
                </div>
                <div className="form-group">
                    <form id="singleForm w-75">
                        <input type="button" style={{ marginLeft: "5px", marginRight: "10px" }} className="btn btn-primary p-2" onClick={this.getAllOrdersHandler} value="Call api get all orders!" />
                        <input type="button" style={{ marginRight: "10px" }} className="btn btn-info p-2" onClick={this.createOrderHandler} value="Call api create order!" />
                        <input type="button" style={{ marginRight: "10px" }} className="btn btn-success p-2" onClick={this.getOrderByIdHandler} value="Call api get order by id!" />
                        <input type="button" style={{ marginRight: "10px" }} className="btn btn-secondary p-2" onClick={this.updateOrderHandler} value="Call api update order!" />
                        <input type="button" style={{ marginRight: "10px" }} className="btn btn-danger p-2" onClick={this.checkVoucherByIdHandler} value="Call api check voucher by id!" />
                        <input type="button" style={{ marginRight: "10px" }} className="btn btn-success p-2" onClick={this.getDrinkListHandler} value="Call api get drink list!" />
                    </form>
                </div>
                <div className="form-group">
                    <br></br>
                    <p id="testP" style={{ marginLeft: "10px", fontSize: "15px" }}> Demo 06 API for Pizza 365 Project: </p>
                    <ul>
                        <li>get all Orders: lấy tất cả orders </li>
                        <li>create Order: tạo 1 order</li>
                        <li>get Order by ID: lấy 1 order bằng ID </li>
                        <li>update Order: update 01 order</li>
                        <li>check voucher by ID: check thông tin mã giảm giá, quan trọng là có hay không, và % giảm giá </li>
                        <li>get drink list: lấy danh sách đồ uống</li>
                    </ul>
                    <strong className="text-danger" style={{ marginLeft: "10px" }}> Bật console log để nhìn rõ output </strong>
                </div>
            </div>
        )
    }
}


export default FetchAPI;